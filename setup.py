# -*- coding: utf-8 -*-
"""
Flask-Settings
------------

Flask extension for additional flask app settings.
"""
from setuptools import setup


setup(
    name='Flask-Settings',
    version='1.1.0',
    url='',
    license='MIT',
    author='Marcin Wojtysiak',
    author_email='wojtysiak.marcin@gmail.com',
    description='Flask extension for additional flask app settings.',
    long_description=__doc__,
    packages=['flask_settings'],
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    install_requires=[],
    tests_require=[],
    test_suite='test_settings',
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)
