# -*- coding: utf-8 -*-
"""
Flask extension for additional flask app settings.

Made by Marcin Wojtysiak 2014
"""
import logging
import copy


class Settings(object):
    """
    Manage system settings
    """

    def __init__(self, app=None, logger_parent='app'):
        """
        Initialize
        """
        # create local logger
        self.logger = logging.getLogger('{0}.settings'.format(logger_parent))
        self.logger.info('init flask-settings')

        # settings model
        self.SETTINGS_MODEL = None

        # database instance
        self.DB_INSTANCE = None

        # secure module instance
        self.SECURE = None

        # settings metadata
        self.metadata = None

        # app instance
        self.app = None

        if app is not None:
            self.init_app(self.app)

    def init_app(self, app):
        """
        Init app
        """
        self.app = app

    def register_db_instance(self, db_instance):
        """Register database instance"""
        self.DB_INSTANCE = db_instance

    def register_settings_model(self, settings_model):
        """
        Register settings models
        """
        self.SETTINGS_MODEL = settings_model

    def register_secure_module(self, secure_module):
        """
        Register module that have encrypt_data and decrypt_data functions
        and take one argument as data to encrypt and decrypt
        """
        self.SECURE = secure_module

    def _get_settings_instance(self, settings_id):
        """Return settings instance"""
        if settings_id is None:
            raise TypeError('settings id can not be None value')

        return self.SETTINGS_MODEL.query.filter(self.SETTINGS_MODEL.settings_id == settings_id).first()

    def load(self):
        """
        Loads system settings and populate them
        """
        try:
            if self.DB_INSTANCE is None:
                raise TypeError('You need to register database instance')

            if self.SETTINGS_MODEL is None:
                raise TypeError('You need to register model')

            if self.SECURE is None:
                raise TypeError('You need to register secure module')

            settings = self.SETTINGS_MODEL.query.all()

            self.metadata = dict()

            for item in settings:
                if item.category not in self.metadata:
                    self.metadata[item.category] = dict()
                self.metadata[item.category][item.key] = dict()
                self.metadata[item.category][item.key]['value'] = item.value
                self.metadata[item.category][item.key]['is_encrypted'] = item.is_encrypted
                self.metadata[item.category][item.key]['id'] = item.settings_id

        except Exception, e:
            self.logger.error(str(e), exc_info=True)
            raise e

    def reload(self):
        """Reload settings"""
        self.load()

    def get_category(self, category):
        """Return all values of category"""

        if self.metadata is None:
            self.load()

        if self.metadata.get(category, None) is None:
            return None

        # make new copy of the result
        all_category = copy.deepcopy(self.metadata[category])

        for key in all_category:
            is_e = all_category[key].get('is_encrypted', False)
            value = all_category[key].get('value', None)

            if is_e:
                if value is not None:
                    all_category[key]['value'] = self.SECURE.decrypt_data(value)

        return all_category

    def get_value(self, category, key):
        """Return dict with two keys (value, is_encrypted)"""
        if self.metadata is None:
            self.load()

        if category not in self.metadata:
            return None

        if self.metadata[category].get(key, None) is None:
            return None

        # make new copy of the result
        value_dict = copy.deepcopy(self.metadata[category][key])

        is_e = value_dict.get('is_encrypted', False)
        value = value_dict.get('value', None)

        if is_e:
            if value is not None:
                value_dict['value'] = self.SECURE.decrypt_data(value)

        return value_dict

    def set_value(self, category, key, value, is_encrypted):
        """
        Save values
        """
        try:
            if self.DB_INSTANCE is None:
                raise TypeError('You need to register database instance')

            if self.SETTINGS_MODEL is None:
                raise TypeError('You need to register model')

            if self.SECURE is None:
                raise TypeError('You need to register secure module')

            if is_encrypted:
                value_to_save = self.SECURE.encrypt_data(value)
            else:
                value_to_save = value

            setting_exist = self.get_value(category, key)

            if setting_exist is None:
                setting = self.SETTINGS_MODEL(category, key, value_to_save, is_encrypted)
                self.DB_INSTANCE.session.add(setting)
                self.DB_INSTANCE.session.commit()
            else:
                setting = self._get_settings_instance(setting_exist.get('id', None))
                setting.value = value_to_save
                setting.is_encrypted = is_encrypted

                self.DB_INSTANCE.session.commit()

            # reload the settings
            self.reload()
        except Exception, e:
            self.logger.error(str(e), exc_info=True)
            raise e

    def set_value_by_id(self, settings_id, value, is_encrypted):
        """"""

        try:
            if self.DB_INSTANCE is None:
                raise TypeError('You need to register database instance')

            if self.SETTINGS_MODEL is None:
                raise TypeError('You need to register model')

            if self.SECURE is None:
                raise TypeError('You need to register secure module')

            if is_encrypted:
                value_to_save = self.SECURE.encrypt_data(value)
            else:
                value_to_save = value

            setting = self._get_settings_instance(settings_id)
            setting.value = value_to_save
            setting.is_encrypted = is_encrypted

            self.DB_INSTANCE.session.commit()

            # reload the settings
            self.reload()
        except Exception, e:
            self.logger.error(str(e), exc_info=True)
            raise e
